(function(win, classes) {
    'use strict';

    describe('Utils', function() {
        var Utils = classes.Utils,
        utils;
        var DataReporter = classes.DataReporter,
        dataReporterInstance;
        var mockChangeEventCheckBox, mockLoadEvent, mockChangeEventGoodEmail,
        mockChangeEventBadEmail, mockChangeEventBadPhoneNumber, mappings;

        describe('email validator', function() {

            beforeEach(function() {
                utils = new Utils();
            });

            it('rejects a number as an email', function() {
                expect(utils.isEmailAddressValid(0)).toBe(false);
            });

            it('rejects a plain string as an email', function() {
                expect(utils.isEmailAddressValid("rejectthisemail")).toBe(false);
            });

            it('rejects an alphanumeric string as an email', function() {
                expect(utils.isEmailAddressValid("reject01thi20semail")).toBe(false);
            });

            it('rejects an alphanumeric and character string as an email', function() {
                expect(utils.isEmailAddressValid("re?ect01th|20sem%@l")).toBe(false);
            });

            it('rejects basic email address missing the TLD', function() {
                expect(utils.isEmailAddressValid("this@test")).toBe(false);
            });

            it('rejects basic email address missing the @', function() {
                expect(utils.isEmailAddressValid("thistest.com")).toBe(false);
            });

            it('accepts a basic email address', function() {
                expect(utils.isEmailAddressValid("this@test.com")).toBe(true);
            });

            it('accepts a basic email address with a dot', function() {
                expect(utils.isEmailAddressValid("this.that@test.com")).toBe(true);
            });

            it('accepts a basic email address with 2 dots', function() {
                expect(utils.isEmailAddressValid("th.isth.at@test.com")).toBe(true);
            });

        });

        describe('phone number validator', function() {

            beforeEach(function() {
                utils = new Utils();
            });

            it('rejects a text string phone number', function() {
                expect(utils.isPhoneNumberValid("abcdefghijk")).toBe(false);
            });

            it('rejects a character string phone number', function() {
                expect(utils.isPhoneNumberValid("@$^£$^£%%@£")).toBe(false);
            });

            it('rejects a large phone number', function() {
                expect(utils.isPhoneNumberValid("0203137573002031375730")).toBe(false);
            });

            it('rejects a small phone number', function() {
                expect(utils.isPhoneNumberValid("0203")).toBe(false);
            });

            it('accepts an 11 digit phone number as text', function() {
                expect(utils.isPhoneNumberValid("00000000000")).toBe(true);
            });

            it('accepts an 11 digit phone number as numbers', function() {
                expect(utils.isPhoneNumberValid(12345678901)).toBe(true);
            });

            it('accepts an 11 digit phone number with spaces', function() {
                expect(utils.isPhoneNumberValid("020 3137 5730")).toBe(true);
            });

            it('accepts an 11 digit phone number with spaces', function() {
                expect(utils.isPhoneNumberValid("02031375730")).toBe(true);
            });

        });

        describe('event handler', function() {

            beforeEach(function() {
                utils = new Utils();
                dataReporterInstance = new DataReporter();
                mappings = [
                    {
                        id: 1,
                        selector: '#email',
                        attribute: 'value',
                        event: 'onChange',
                        isEmail: true,
                        isPhoneNumber: false
                    },
                    {
                        id: 6,
                        selector: '#termsAndConditions',
                        attribute: 'checkbox',
                        event: 'onChange',
                        isEmail: false,
                        isPhoneNumber: false
                    },
                    {
                        id: 7,
                        selector: '.productName',
                        attribute: 'text',
                        event: 'onLoad',
                        isEmail: false,
                        isPhoneNumber: false
                    },
                    {
                        id: 5,
                        selector: '#phoneNumber',
                        attribute: 'value',
                        event: 'onChange',
                        isEmail: false,
                        isPhoneNumber: true
                    }
                ];
                mockChangeEventCheckBox = { target: {checked: true} };
                mockChangeEventGoodEmail = { target: {value: "test@email.com"} };
                mockChangeEventBadEmail = { target: {value: "testemailcom"} };
                mockChangeEventBadPhoneNumber = { target: {value: "notaphonenumber"} };
                mockLoadEvent = { target: {innerText: "Lamp"} };
            });

            it('sends data on change of email field', function() {
                console.log = jasmine.createSpy("log");
                utils.elementEventHandler(mockChangeEventGoodEmail, null, mappings[0], dataReporterInstance);
                expect(console.log).toHaveBeenCalledWith("dataCaptured: mapping id: 1 - data: test@email.com");
            });

            it('does not send data on change for bad email address', function() {
                console.log = jasmine.createSpy("log");
                utils.elementEventHandler(mockChangeEventBadEmail, null, mappings[0], dataReporterInstance);
                expect(console.log).toHaveBeenCalledWith("Email address is invalid");
            });

            it('does not send data on change for bad phone number', function() {
                console.log = jasmine.createSpy("log");
                utils.elementEventHandler(mockChangeEventBadPhoneNumber, null, mappings[3], dataReporterInstance);
                expect(console.log).toHaveBeenCalledWith("Phone number is invalid");
            });

            it('sends data on change of checkbox field', function() {
                console.log = jasmine.createSpy("log");
                utils.elementEventHandler(mockChangeEventCheckBox, null, mappings[1], dataReporterInstance);
                expect(console.log).toHaveBeenCalledWith("dataCaptured: mapping id: 6 - data: checked");
            });

            it('sends data on load of element', function() {
                console.log = jasmine.createSpy("log");
                utils.elementEventHandler(mockLoadEvent, null, mappings[2], dataReporterInstance);
                expect(console.log).toHaveBeenCalledWith("dataCaptured: mapping id: 7 - data: Lamp");
            });

        });

    });
}(window, classes));
