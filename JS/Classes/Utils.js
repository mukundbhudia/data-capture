var classes = classes || {};

(function(win, classes){
	'use strict';

	/**
	 * Utilities class for event handling and validation.
	 *
	 * @constructs Utils
	 */
	function Utils() {};

	/**
	 * Handles the events for each web page element for the form
	 * @param  {Object} event                Event object passed from the browser
	 * @param  {Object} elementForSelector   Element object for the event to be handelled from
	 * @param  {Object} mapping              The mapping object from the mappings array
	 * @param  {Object} dataReporterInstance The DataReporter object instance
	 * @return {void}                        None
	 */
	Utils.prototype.elementEventHandler = function(event, elementForSelector, mapping, dataReporterInstance) {
		var elementValue = null;
		var targetElement = null;

		//When a manual trigger of this even handler is called the event is null...
		//...and a manual element is passed in
		if (event === null) {
			targetElement = elementForSelector;
		} else if (elementForSelector === null) {
			targetElement = event.target;
		}

		//Determine the type of data being handled and assign the values
		//according to the type of element
		if (mapping.attribute === 'checkbox') {
			elementValue = targetElement.checked;
			if (elementValue === true) {
				elementValue = 'checked';
			} else {
				elementValue = 'unchecked';
			}
		} else if (mapping.attribute === 'text') {
			elementValue = targetElement.innerText;
		} else {
			elementValue = targetElement.value;
		}

		//Validate according to mapping flags
		if (true === mapping.isEmail) {
			if (true === this.isEmailAddressValid(elementValue)) {
				dataReporterInstance.send(mapping.id, elementValue);
			} else {
				console.log("Email address is invalid");
			}
		} else if (true === mapping.isPhoneNumber) {
			if (true === this.isPhoneNumberValid(elementValue)) {
				dataReporterInstance.send(mapping.id, elementValue);
			} else {
				console.log("Phone number is invalid");
			}
		} else {
			dataReporterInstance.send(mapping.id, elementValue);
		}

	};

	/**
	 * Checks whether a given object is an email number
	 * @param  {String}  emailAddress Email address
	 * @return {Boolean}              Whether the number is valid
	 */
	Utils.prototype.isEmailAddressValid = function(emailAddress) {
		var emailValidationRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (emailValidationRegex.test(emailAddress)) {
			return true;
		}
		return false;
	};

	/**
	 * Checks whether a given object is a phone number
	 * @param  {String}  phoneNumber UK phone number
	 * @return {Boolean}             Whether the number is valid
	 */
	Utils.prototype.isPhoneNumberValid = function(phoneNumber) {
		var phoneNumberValidationRegex = /^(?:\W*\d){11}\W*$/;
		if (phoneNumberValidationRegex.test(phoneNumber)) {
			return true;
		}
		return false;
	};

	classes.Utils = Utils;

}(window, classes));
