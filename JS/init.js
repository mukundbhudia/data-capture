(function () {
	'use strict';

	/**
	* This function is responsible for capturing data (based on mappings.js) and sending data (with use of DataReporter class).
	*
	* @name init
	*/
	function init () {
		var DataReporter = classes.DataReporter;
		var Utils = classes.Utils;
		var dataReporterInstance = new DataReporter();
		var utils = new Utils();
		var elements = [];

		for (var i = 0; i < mappings.length; i++) {
			//Go through each mapping selector and find all corresponding array of elements
			var elementForSelector = document.querySelectorAll(mappings[i].selector);
			//If there's only one element for the selector, we add it to an array of elements
			if (elementForSelector.length === 1) {
				elements.push({
					mapping: mappings[i],
					element: elementForSelector[0]
				});
			//If theres more than one element for the mapper selector then we go
			//through each of those elements and add to the array of elements
			} else if (elementForSelector.length > 1) {
				for (var j = 0; j < elementForSelector.length; j++) {
					elements.push({
						mapping: mappings[i],
						element: elementForSelector[j]
					});
				}
			}

		}

		//We go through all encountered elements and bind event handlers to them
		for (var i = 0; i < elements.length; i++) {
			//Each element has a separate event handler with specific dataReporter instances
			//and mappings so we wrap the following in a closure
			(function() {
				var domEventFromMapping = elements[i].mapping.event.split('on')[1].toLowerCase();
				var elementForSelector = document.querySelectorAll(elements[i].mapping.selector)[0];
				var elementId = elements[i].mapping.id;
				var mapping = elements[i].mapping;

				//Special case as load is not bound to an element and has to be
				//triggered straight away
				if (domEventFromMapping === 'load') {
					utils.elementEventHandler(null, elementForSelector, mapping, dataReporterInstance);
				}

				elements[i].element.addEventListener(domEventFromMapping, function(event) {
					utils.elementEventHandler(event, null, mapping, dataReporterInstance);
				}, false);

			}());

		}
	}

	document.addEventListener('DOMContentLoaded', init, false);

}());
