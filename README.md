# data-capture

Goal: capture data present on a page in order to report them to an API.

### What you need before you run the app: ###

* NodeJS

### How do I install? ###

Once cloned/downloaded cd to directory and:

```
npm install
```

### How do I test? ###
```
npm test
```
and wait for a karma test runner URL to appear on the console.
You can also test the app by entering:

```
./node_modules/karma/bin/karma start
```
### References: ###

* Email address validation RegEx: http://www.w3resource.com/javascript/form/email-validation.php
* Phone number validation RegEx: https://stackoverflow.com/questions/5286046/javascript-phone-number-validation
